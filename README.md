# Hihope文档

欢迎访问Hihope文档仓库，参与Hihope开发者文档开源项目，与我们一起完善开发者文档。

此仓库存放Hihope社区提供的产品资料、设备开发、应用开发对应的开发者文档。




## Hihope文档说明

### 产品列表

* [Neptune ](Neptune)：高性价比Wi-Fi&蓝牙双模SoC模组

* [HiSpark WiFi IoT](HiSpark_WiFi_IoT)：HiSpark WiFi IoT智能家居开发套件基于海思Hi3861芯片设计，套件包含丰富的功能单板及配件模块

* [Pegasus Smart Car](Pegasus-Smart-Car)：Pegasus Smart Car智能小车开发套件基于海思Hi3861芯片设计，套件包含丰富的功能单板及配件模块

* [HiSpark AI Camera Developer Kit](HiSpark-AI-Camera-Developer-Kit)：HiSpark AI Camera开发套件基于海思Hi3516芯片设计，套件包含丰富的功能单板及配件模块

* [HiSpark IPC DIY Camera](HiSpark-IPC-DIY-Camera)：Aries IPC开发套件基于海思Hi3518芯片设计，套件包含丰富的功能单板及配件模块

* [Pegasus IoT Developer Kit](Pegasus-IoT-Developer-Kit)：Pegasus物联网教学套件基于海思Hi3861芯片设计，套件包含丰富的功能单板及配件模块

* [Pegasus Microprocessor Kit](Pegasus-Microprocessor-Kit)：Pegasus微处理器应用教学套件基于海思Hi3861芯片设计，套件包含丰富的功能单板及配件模块

* [DAYU 200](HiHope_DAYU200)：基于RK3568设计的DAYU 200开发板

  

## 购买渠道

官方淘宝店铺：【润和芯片社区HopeRun】HiSpark开发套件购买链接：

| 产品                                                         | 链接                                             |
| ------------------------------------------------------------ | ------------------------------------------------ |
| ① HarmonyOS HiSpark AI Camera开发套件 （Hi3516DV300）        | https://item.taobao.com/item.htm?id=622922688823 |
| ② HarmonyOS HiSpark IPC DIY开发套件 （Hi3518EV300）          | https://item.taobao.com/item.htm?id=623376454933 |
| ③ HarmonyOS HiSpark Wi-Fi IoT智能家居开发套件 （Hi3861V100） | https://item.taobao.com/item.htm?id=622343426064 |
| ④ Neptune HarmonyOS 物联网 IOT模组                           | https://item.taobao.com/item.htm?id=635868903111 |



## 特别感谢

[HUAWEI DevEco Device Tool](https://device.harmonyos.com/cn/ide#download_release)对Hihope开发板的支持


