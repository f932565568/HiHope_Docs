# WiFi连接——STA模式

WiFi连接的代码示例是 wm_sta_demo.c

## 1.测试步骤

手机设置热点，或者将路由器的热点名称改成 hihope

密码改成12345678

在UserMain函数最后面调用w800_sta_demo函数

```
void UserMain(void)
{
    printf("\n user task \n");
#if DEMO_CONSOLE
    CreateDemoTask();
#endif

#if defined(LOSCFG_KERNEL_TEST_FULL) || defined(LOSCFG_KERNEL_TEST)
	LosAppInit(); 
#else
    if (DeviceManagerStart()) {
        printf("[%s] No drivers need load by hdf manager!",__func__);
    }
#endif	
    w800_sta_demo();
}
```

编译烧录后查看打印信息：

![image-20220516211745062](image/image-20220516211745062.png)

说明WiFi连接成功



## 2.代码部分

```

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "lwip/ip4_addr.h"
#include "lwip/netif.h"
#include "lwip/netifapi.h"

#include "cmsis_os2.h"
#include "ohos_init.h"
#include "wifi_device.h"
#include "wifi_error_code.h"


#define DEF_TIMEOUT 15
#define ONE_SECOND 1
#define DHCP_DELAY 100

static int WiFiInit(void);
static void WaitScanResult(void);
static int WaitConnectResult(void);
static void OnWifiScanStateChangedHandler(int state, int size);
static void OnWifiConnectionChangedHandler(int state, WifiLinkedInfo *info);
static void OnHotspotStaJoinHandler(StationInfo *info);
static void OnHotspotStateChangedHandler(int state);
static void OnHotspotStaLeaveHandler(StationInfo *info);
static void PrintLinkedInfo(WifiLinkedInfo* info);

static int g_staScanSuccess = 0;
static int g_connectSuccess = 0;
static int g_ssid_count = 0;
static struct netif *g_lwip_netif = NULL;
static WifiEvent g_wifiEventHandler = { 0 };
static WifiErrorCode error;

#define SELECT_WLAN_PORT "wlan0"

#define SELECT_WIFI_SSID "hihope"
#define SELECT_WIFI_PASSWORD "12345678"
#define SELECT_WIFI_SECURITYTYPE WIFI_SEC_TYPE_PSK



static int WifiConnectAp(const char *ssid, const char *psk, WifiScanInfo *info, int i)
{
    if (strcmp(ssid, info[i].ssid) == 0) {
        int result;
        printf("Select:%3d wireless, Waiting...\r\n", i + 1);


        WifiDeviceConfig select_ap_config = { 0 };
        strcpy_s(select_ap_config.ssid, sizeof(select_ap_config.ssid), info[i].ssid);
        strcpy_s(select_ap_config.preSharedKey, sizeof(select_ap_config.preSharedKey), psk);
        select_ap_config.securityType = WIFI_SEC_TYPE_PSK;

        if (AddDeviceConfig(&select_ap_config, &result) == WIFI_SUCCESS) {
            if (ConnectTo(result) == WIFI_SUCCESS && WaitConnectResult() == 1) {
                g_lwip_netif = netifapi_netif_find_by_name(SELECT_WLAN_PORT);
                return 0;
            }
        }
    }
    return -1;
}


static void wm_sta_task(void)
{
    unsigned int size = WIFI_SCAN_HOTSPOT_LIMIT;


    if (WiFiInit() != WIFI_SUCCESS) {
        printf("WiFiInit failed, error = %d\r\n", error);
        return -1;
    }

    WifiScanInfo *info = malloc(sizeof(WifiScanInfo) * WIFI_SCAN_HOTSPOT_LIMIT);
    memset(info, 0, (sizeof(WifiScanInfo) * WIFI_SCAN_HOTSPOT_LIMIT));
    if (info == NULL) {
        return -1;
    }

    do {
        Scan();
        WaitScanResult();
        error = GetScanInfoList(info, &size);
    } while (g_staScanSuccess != 1);

    printf("********************\r\n");
    for (uint8_t i = 0; i < g_ssid_count; i++) {
        printf("no:%03d, ssid:%-30s, rssi:%5d\r\n", i + 1, info[i].ssid, info[i].rssi);
    }
    printf("********************\r\n");

    for (uint8_t i = 0; i < g_ssid_count; i++) {
        if (WifiConnectAp(SELECT_WIFI_SSID, SELECT_WIFI_PASSWORD, info, i) == WIFI_SUCCESS) {
            printf("WiFi connect succeed!\r\n");
            break;
        }

        if (i == g_ssid_count - 1) {
            printf("ERROR: No wifi as expected\r\n");
            while (1)
                osDelay(DHCP_DELAY);
        }
    }


    if (g_lwip_netif) {
        dhcp_start(g_lwip_netif);
        printf("begain to dhcp\r\n");
    }

    for (;;) {
        if (dhcp_is_bound(g_lwip_netif) == ERR_OK) {
            printf("<-- DHCP state:OK -->\r\n");

            break;
        }
        osDelay(DHCP_DELAY);
    }


    for (;;) {
        osDelay(DHCP_DELAY);
    }
}

int WiFiInit(void)
{
    g_wifiEventHandler.OnWifiScanStateChanged = OnWifiScanStateChangedHandler;
    g_wifiEventHandler.OnWifiConnectionChanged = OnWifiConnectionChangedHandler;
    g_wifiEventHandler.OnHotspotStaJoin = OnHotspotStaJoinHandler;
    g_wifiEventHandler.OnHotspotStaLeave = OnHotspotStaLeaveHandler;
    g_wifiEventHandler.OnHotspotStateChanged = OnHotspotStateChangedHandler;
    error = RegisterWifiEvent(&g_wifiEventHandler);
    if (error != WIFI_SUCCESS) {
        printf("register wifi event fail!\r\n");
        return -1;
    }

    if (EnableWifi() != WIFI_SUCCESS) {
        printf("EnableWifi failed, error = %d\r\n", error);
        return -1;
    }

    if (IsWifiActive() == 0) {
        printf("Wifi station is not active.\r\n");
        return -1;
    }
    return 0;
}

static void OnWifiScanStateChangedHandler(int state, int size)
{
    (void)state;
    if (size > 0) {
        g_ssid_count = size;
        g_staScanSuccess = 1;
    }
    return;
}

static void PrintLinkedInfo(WifiLinkedInfo* info)
{
    if (!info) return;

    static char macAddress[32] = {0};
    unsigned char* mac = info->bssid;
    snprintf(macAddress, sizeof(macAddress), "%02X:%02X:%02X:%02X:%02X:%02X",
        mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    printf("bssid: %s, rssi: %d, connState: %d, reason: %d, ssid: %s\r\n",
        macAddress, info->rssi, info->connState, info->disconnectedReason, info->ssid);
}

static void OnWifiConnectionChangedHandler(int state, WifiLinkedInfo *info)
{
    (void)info;

    if (state > 0) {
        g_connectSuccess = 1;

        //PrintLinkedInfo(info);

        printf("callback function for wifi connect\r\n");
    } else {
        printf("connect error,please check password\r\n");
    }
    return;
}

static void OnHotspotStaJoinHandler(StationInfo *info)
{
    (void)info;
    printf("STA join AP\n");
    return;
}

static void OnHotspotStaLeaveHandler(StationInfo *info)
{
    (void)info;
    printf("HotspotStaLeave:info is null.\n");
    return;
}

static void OnHotspotStateChangedHandler(int state)
{
    printf("HotspotStateChanged:state is %d.\n", state);
    return;
}

static void WaitScanResult(void)
{
    int scanTimeout = DEF_TIMEOUT;
    while (scanTimeout > 0) {
        sleep(ONE_SECOND);
        scanTimeout--;
        if (g_staScanSuccess == 1) {
            printf("WaitScanResult:wait success[%d]s\n", (DEF_TIMEOUT - scanTimeout));
            break;
        }
    }
    if (scanTimeout <= 0) {
        printf("WaitScanResult:timeout!\n");
    }
}

static int WaitConnectResult(void)
{
    int ConnectTimeout = DEF_TIMEOUT;
    while (ConnectTimeout > 0) {
        sleep(1);
        ConnectTimeout--;
        if (g_connectSuccess == 1) {
            printf("WaitConnectResult:wait success[%d]s\n", (DEF_TIMEOUT - ConnectTimeout));
            break;
        }
    }
    if (ConnectTimeout <= 0) {
        printf("WaitConnectResult:timeout!\n");
        return 0;
    }

    return 1;
}


void w800_sta_demo(void)
{
    osThreadAttr_t attr;

    attr.name = "wm_sta_task";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 10240;
    attr.priority = 26;

    if (osThreadNew((osThreadFunc_t)wm_sta_task, NULL, &attr) == NULL) {
        printf("Falied to create wm_sta_task!\r\n");
    }
}
```

