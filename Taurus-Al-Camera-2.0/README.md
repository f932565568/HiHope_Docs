

## 一．产品简介

Taurus Al Camera 2.0开发套件基于华为海思Hi3516DV300芯片，支持OpenHarmony、LiteOS、Linux等操作系统。

套件集成索尼高端安防摄像头、5.5寸触摸显示屏、HDMI接口、音频接口、SD卡接口、网卡等，同时引出扩展接口，包括I2C、UART、GPIO、PWM、ADC模拟采集等，支持NFC模组、5G模组、舵机等扩展。可实现图像采集、图像识别、双屏显示、双向语音等功能，广泛应用于智能摄像、安防监控、车载记录仪等。

![image-20220507182205854](image/image-20220507182205854.png)

## 二．产品亮点

l **专用Smart HD IP** **C****amera芯片**

Hi3516DV300集成新一代ISP、业界最新的 H.265 视频压缩编码器，高性能NNIE引擎，1.0TOPS

l **多操作系统**

支持OpenHarmony、LiteOS、Linux多种操作系统

l **灵活的存储空间**

标配32bit/1GB DDR3，最大可支持32bit/2GB处理空间；标配8GB eMMC存储器，同时可外挂2TB SDXC卡

l **丰富的通信接口**

灵活的Type-C通信兼容供电，可选择UART及JTAG、Ethernet调试接口，同步WiFi通信

l **安全性、智能化处理分析**

预留算法加密IC，为独立版权保驾护航

## 三．产品参数

| 配置           | 参数                                                         |
| -------------- | ------------------------------------------------------------ |
| SoC            | l 基于海思专用Smart HD IP Camera SoC ：Hi3516DV300l 双核Cortex-A7 MP2 @900MHz处理器l 集成新一代 ISP、高性能NNIE引擎，1.0Tops |
| 操作系统       | l 支持OpenHarmony、LiteOS、Linux多操作系统                   |
| 通信接口       | l 1组DDRC，最大支持32bit/2GB处理空间，标准产品配套1GB DDR3，最大数据数率1.8Gbpsl eMMC4.5，最大支持4bit/64GB存储空间，标准产品配套8GBl 外部扩展SD存储接口，最大支持2TB SDXC卡 |
| 功能特性       | l 低功耗WiFi无线通信l Ethernet通信，RMII模式l Type-C接口，支持USB2.0＋Type-C调试线缆 |
| 调试接口       | l JTAG接口l UART0 Debug接口+串口转接线l 100M网口调试         |
| 视频与图形处理 | **输入**l 索尼高端安防低照度Sensor IMX335：5.04M像素，最大分辨率2592(H)x1944(V),60fps帧数率l 星光级黑光低照度M12镜头：F1.6大光圈，1/2.7”成像靶面，3.6mm焦距l 高精度光敏检测，补光＋IR-Cut红外夜视功能**输出**l H.265视频压缩编码输出图形和视频1/15~16X缩放l 图像90°/180°/270°旋转l MicroHDMI接口输出高清画质，最大分辨率1080P60l 兼容4寸、5.5寸LCD/TP屏显示 |
| 音频特性       | l 单声道驻极体表贴mic，同步预留差分输入mic接口l 2030腔体全频段喇叭，配1.25间距端子，位置自由调整 |
| 功能扩展       | l 蜂鸣器、双色指示灯音色搭配，双提示功能l 2路自定义按键、I2C、UART、GPIO、PWM、ADC模拟采集等，支持NFC模组、5G模组、舵机等扩展 |
| 安全性         | l 预留算法加密ICl 3C安全认证、带开关按键、高可靠性Type-C接口5V/3A电源适配器 |