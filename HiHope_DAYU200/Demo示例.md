# 验证拍照录像功能

## 前提条件

1.编译hdc工具

```
编译命令：./build.sh --product-name ohos-sdk   #在源码根路径下执行
获取路径：成功编译后，可在路径下./out/sdk/ohos-sdk/linux/toolchains/ #下载hdc_std到本地
```

注：可将hdc_std重命名为hdc

2.在系统环境变量里面配置hdc工具的路径

2.打开cmd，执行hdc -v，输出版本号（例如：Ver: 1.1.1e）hdc工具即可生效

## 测试步骤

1. hdc shell 进入shell命令行，进入 system/bin路径下，运行 ohos_camera_demo 
2. 根据demo工具提示，测试拍照（-c）、录像（-v）功能 

```
Options:
-h | --help          Print this message
-o | --offline       stream offline test
-c | --capture       capture one picture
-w | --set WB        Set white balance Cloudy
-v | --video         capture Viedeo of 10s
-a | --Set AE        Set Auto exposure
-f | --Set Flashlight        Set flashlight ON 5s OFF
-q | --quit          stop preview and quit this app
```

3. 从/data目录下，通过hdc file recv命令导出拍照文件和录像文件到本地文件夹，查看拍照和录像文件

```
hdc file recv /data/照片 E:\WorkFiles\camera
```

